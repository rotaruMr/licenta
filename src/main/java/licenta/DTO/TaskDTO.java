package licenta.DTO;
import licenta.entity.Project;

public class TaskDTO {

	public String codeTask;
	public String name;
	public String description;
	public Project project;
	
	public String getCodeTask() {
		return codeTask;
	}
	public void setCodeTask(String codeTask) {
		this.codeTask = codeTask;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public TaskDTO(String codeTask, String name, String description, Project project) {
		super();
		this.codeTask = codeTask;
		this.name = name;
		this.description = description;
		this.project = project;
	}
	public TaskDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
