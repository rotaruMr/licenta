package licenta.DTO;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewEmployeeDTO 
{
	@NotNull
	@Size(min=2,max=60,message="First Name must be 2 or more characters in length")
	public String firstName;
	@NotNull
	@Size(min=2,max=60,message="Last Name must be 2 or more characters in length")
	public String lastName;
	@NotNull
	@Email(regexp="^(.+)@(.+)$",message="Invalid email")
	public String email;
	
	public NewEmployeeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NewEmployeeDTO(String firstName, String lastName, String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
