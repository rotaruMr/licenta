package licenta.exceptions;

public class NotUniqueCodeException extends Exception {

	private static final long serialVersionUID = 1L;


	public NotUniqueCodeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotUniqueCodeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public NotUniqueCodeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotUniqueCodeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotUniqueCodeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
