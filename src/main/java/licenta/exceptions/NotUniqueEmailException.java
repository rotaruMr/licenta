package licenta.exceptions;

public class NotUniqueEmailException extends Exception {

	private static final long serialVersionUID = 1L;


	public NotUniqueEmailException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotUniqueEmailException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public NotUniqueEmailException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotUniqueEmailException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotUniqueEmailException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
