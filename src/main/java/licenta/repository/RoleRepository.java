package licenta.repository;

import org.springframework.data.repository.CrudRepository;

import licenta.entity.Role;

public interface RoleRepository extends CrudRepository<Role, Integer>{

}
