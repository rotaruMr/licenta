
package licenta.repository;

import org.springframework.data.repository.CrudRepository;

import licenta.entity.Task;

public interface TaskRepository extends CrudRepository<Task, Integer>{

}

