package licenta.repository;

import org.springframework.data.repository.CrudRepository;

import licenta.entity.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>{

}
