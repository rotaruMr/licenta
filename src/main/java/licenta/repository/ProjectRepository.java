package licenta.repository;

import org.springframework.data.repository.CrudRepository;

import licenta.entity.Project;


public interface ProjectRepository extends CrudRepository<Project, Integer>{

}
