package licenta.repository;

import org.springframework.data.repository.CrudRepository;

import licenta.entity.UserTimesheet;

public interface UserRepository extends CrudRepository<UserTimesheet, Integer>{

}
