package licenta.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IndexController {
	@GetMapping(value= {"index"})
	public String getIndex() {
		return "index";
	}
	@PostMapping(value= {"index"})
	public String postIndex() {
		return "index";
	}
}
