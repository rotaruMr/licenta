package licenta.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import licenta.DTO.ProjectDTO;
import licenta.Utils.NormalizeText;
import licenta.entity.Project;
import licenta.exceptions.NotUniqueCodeException;
import licenta.exceptions.NotUniqueEmailException;
import licenta.exceptions.WrongInputDataException;
import licenta.model.ProjectViewModel;
import licenta.repository.ProjectRepository;

@Controller
public class ProjectController {

	@Autowired
	private ProjectRepository projectRepo;

	private List<Project> getAllProjects() {
		final Iterable<Project> list = projectRepo.findAll();
		final List<Project> searchedList = new ArrayList<>();
		for (final Project project : list) {
			project.setName(NormalizeText.normalizeString(project.getName()));
			project.setDescription(NormalizeText.normalizeString(project.getDescription()));
			searchedList.add(project);
		}
		return searchedList;
	}

	private Boolean checkUniqueCode(List<Project> projects, String code) {
		for (Project project : projects)
			if (project.getCode().equals(code))
				return false;
		return true;
	}

	@GetMapping("/projectsView")
	public String getAllProjects(Model model) {
		model.addAttribute("projectViewModel", new ProjectViewModel(getAllProjects()));
		Boolean addProjectIsAvailable = false;
		model.addAttribute("addProjectIsAvailable", addProjectIsAvailable);
		return "views/projectsView";
	}

	@PostMapping("/projectsView")
	public String postAllProjects(Model model, @ModelAttribute @Valid ProjectDTO projectDTO,
			@RequestParam MultiValueMap<String, String> params)
			throws WrongInputDataException, NotUniqueEmailException {
		Boolean addProjectIsAvailable = false;
		model.addAttribute("errorMessage", false);
		model.addAttribute("addProjectIsAvailable", addProjectIsAvailable);
		model.addAttribute("projectViewModel", new ProjectViewModel(getAllProjects()));
		model.addAttribute("projectDTO", new ProjectDTO());
		String idProject = null;

		for (String key : params.keySet()) {
			idProject = key;
		}

		String reqParam = params.getFirst(idProject);
		System.out.println(reqParam);
		switch (reqParam) {
		case "Add":
			addProjectIsAvailable = true;
			model.addAttribute("addProjectIsAvailable", addProjectIsAvailable);
			break;
		case "Cancel":
			addProjectIsAvailable = false;
			model.addAttribute("addProjectIsAvailable", addProjectIsAvailable);
		case "Save":
			List<Project> projects = getAllProjects();
			Project project = new Project();
			try {

				if (projectDTO.getCodeProject().isEmpty() || projectDTO.getName().isEmpty()
						|| projectDTO.getStartDate().isEmpty() || projectDTO.getDescription().isEmpty()) {
					model.addAttribute("errorMessage", true);
					throw new WrongInputDataException();
				} else {
					Boolean uniqueCodeError = false;
					if (!checkUniqueCode(projects, projectDTO.getCodeProject())) {
						uniqueCodeError = true;
						model.addAttribute("uniqueCodeError", uniqueCodeError);
						throw new NotUniqueCodeException();
					} else {
						model.addAttribute("uniqueCodeError", uniqueCodeError);
					}
					project.setCode(projectDTO.getCodeProject());
					project.setName(NormalizeText.normalizeString(projectDTO.getName()));
					project.setStart_date(projectDTO.getStartDate());
					project.setEnd_date(projectDTO.getEndDate());
					project.setDescription(NormalizeText.normalizeString(projectDTO.getDescription()));
				}
				projectRepo.save(project);
				addProjectIsAvailable = false;
				model.addAttribute("addProjectIsAvailable", addProjectIsAvailable);
				model.addAttribute("projectViewModel", new ProjectViewModel(getAllProjects()));

			} catch (NotUniqueCodeException | WrongInputDataException e) {
				// TODO: handle exception
			}
			break;
		case "End":
			addProjectIsAvailable = false;
			model.addAttribute("addProjectIsAvailable", addProjectIsAvailable);
			List<Project> projectsList = getAllProjects();
			for (Project item : projectsList) {
				if (item.getId() == Integer.parseInt(idProject)) {
					item.setEnd_date(java.time.LocalDate.now().toString());
					projectRepo.save(item);
				}
			}
			model.addAttribute("projectViewModel", new ProjectViewModel(getAllProjects()));

		}
		return "views/projectsView";
	}
}
