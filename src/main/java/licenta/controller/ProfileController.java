package licenta.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ProfileController {
	@GetMapping(value= {"profile"})
	public String getProfile() {
		return "views/profile";
	}
	@PostMapping(value= {"profile"})
	public String postProfile() {
		return "views/profile";
	}
}
