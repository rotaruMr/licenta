package licenta.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegisterController {


	@GetMapping("/register")
	public String getRegister() {
		return "views/registerView";
	}
	@PostMapping("/register")
	public String postRegister() {
		return "views/registerView";
	}

}
