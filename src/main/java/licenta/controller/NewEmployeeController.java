package licenta.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import licenta.DTO.NewEmployeeDTO;
import licenta.Utils.NormalizeText;
import licenta.entity.Employee;
import licenta.exceptions.NotUniqueEmailException;
import licenta.exceptions.WrongInputDataException;
import licenta.model.NewEmployeeViewModel;
import licenta.repository.EmployeeRepository;

@Controller
public class NewEmployeeController {
	@Autowired
	private EmployeeRepository employeeRepo;

	private List<Employee> getAllEmployees() {
		final Iterable<Employee> list = employeeRepo.findAll();
		final List<Employee> searchedList = new ArrayList<>();
		for (final Employee employee : list) {
			employee.setFirst_name(NormalizeText.normalizeString(employee.getFirst_name()));
			employee.setLast_name(NormalizeText.normalizeString(employee.getLast_name()));
			employee.setEmail(employee.getEmail());
			searchedList.add(employee);
		}
		return searchedList;
	}

	private Boolean checkUniqueEmail(List<Employee> employees, String email) {
		for (Employee employee : employees)
			if (employee.getEmail().equals(email))
				return false;
		return true;
	}

	@GetMapping("/employeesView")
	public String getAllEmployees(Model model) {
		model.addAttribute("newEmployeeViewModel", new NewEmployeeViewModel(getAllEmployees()));
		Boolean addEmployeeIsAvailable = false;
		model.addAttribute("addEmployeeIsAvailable", addEmployeeIsAvailable);
		return "views/employeesView";
	}

	@PostMapping("/employeesView")
	public String postAllEmployees(Model model, @ModelAttribute @Valid NewEmployeeDTO newEmployeeDTO,
			BindingResult bindingResult, @RequestParam("submit") String reqParam,
			@RequestParam(value = "delete", required = false) String employeeToDelete)
			throws WrongInputDataException, NotUniqueEmailException {
		System.out.println(employeeToDelete);
		model.addAttribute("errorMessage", false);
		Boolean addEmployeeIsAvailable = false;
		model.addAttribute("addEmployeeIsAvailable", addEmployeeIsAvailable);
		model.addAttribute("newEmployeeViewModel", new NewEmployeeViewModel(getAllEmployees()));
		model.addAttribute("newEmployeeDTO", new NewEmployeeDTO());
		switch (reqParam) {
		case "Add":
			addEmployeeIsAvailable = true;
			model.addAttribute("addEmployeeIsAvailable", addEmployeeIsAvailable);
			break;
		case "Save":
			List<Employee> employees = getAllEmployees();
			Employee employee = new Employee();
			try {

				if (newEmployeeDTO.getFirstName().isEmpty() || newEmployeeDTO.getLastName().isEmpty()
						|| newEmployeeDTO.getEmail().isEmpty()) {
					model.addAttribute("errorMessage", true);
					throw new WrongInputDataException();
				} else {
					Boolean uniqueEmailError = false;
					if (!checkUniqueEmail(employees, newEmployeeDTO.getEmail())) {
						uniqueEmailError = true;
						model.addAttribute("uniqueEmailError", uniqueEmailError);
						throw new NotUniqueEmailException();
					} else {
						model.addAttribute("uniqueEmailError", uniqueEmailError);
					}
					employee.setFirst_name(NormalizeText.normalizeString(newEmployeeDTO.getFirstName()));
					employee.setLast_name(NormalizeText.normalizeString(newEmployeeDTO.getLastName()));
					employee.setEmail(newEmployeeDTO.getEmail());
				}
				employeeRepo.save(employee);
				addEmployeeIsAvailable = false;
				model.addAttribute("addEmployeeIsAvailable", addEmployeeIsAvailable);
				model.addAttribute("employeeViewModel", new NewEmployeeViewModel(getAllEmployees()));

			} catch (NotUniqueEmailException | WrongInputDataException e) {
				// TODO: handle exception
			}

			break;
		case "Delete":
			addEmployeeIsAvailable = false;

			if (employeeToDelete != null) 
			{
				List<Integer> toDelete = convertToList(employeeToDelete);
				for(Integer index : toDelete)
				{
					if (employeeRepo.existsById(index));
					{
						model.addAttribute("deleteEmployee", true);
						employeeRepo.deleteById(index);
					}
				}
			} 
			else 
			{
				model.addAttribute("deleteEmployee", false);
			}
			model.addAttribute("addEmployeeIsAvailable", addEmployeeIsAvailable);
			model.addAttribute("employeeViewModel", new NewEmployeeViewModel(getAllEmployees()));
		case "Cancel":
			addEmployeeIsAvailable = false;
			model.addAttribute("addEmployeeIsAvailable", addEmployeeIsAvailable);
		}
		return "views/employeesView";
	}

	private List<Integer> convertToList(String toDelete) {
		List<Integer> listOfIndexes = new ArrayList<>();
		String[] tokens = toDelete.split(",");
		for (String token : tokens) {
			listOfIndexes.add(Integer.parseInt(token));
		}
		return listOfIndexes;
	}
}
