package licenta.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {
	
	@GetMapping(value= {"/","login"})
	public String getLogin() {
		return "views/loginView";
	}
	
	@PostMapping(value= {"/","login"})
	public String postLogin() {
		return "views/loginView";
	}

}
