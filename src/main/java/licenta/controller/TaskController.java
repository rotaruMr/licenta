package licenta.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import licenta.DTO.TaskDTO;
import licenta.Utils.NormalizeText;
import licenta.entity.Project;
import licenta.entity.Task;
import licenta.exceptions.NotUniqueCodeException;
import licenta.exceptions.NotUniqueEmailException;
import licenta.exceptions.WrongInputDataException;
import licenta.model.ProjectViewModel;
import licenta.model.TaskViewModel;
import licenta.repository.ProjectRepository;
import licenta.repository.TaskRepository;

@Controller
public class TaskController {

	@Autowired
	private TaskRepository tasksRepo;

	@Autowired
	private ProjectRepository projectsRepo;

	private List<Task> getAllTasks() {
		final Iterable<Task> list = tasksRepo.findAll();
		final List<Task> searchedList = new ArrayList<>();
		for (final Task task : list) {
			searchedList.add(task);
		}
		return searchedList;
	}

	private List<Project> getAllProjects() {
		final Iterable<Project> list = projectsRepo.findAll();
		final List<Project> searchedList = new ArrayList<>();
		for (final Project project : list) {
			project.setName(NormalizeText.normalizeString(project.getName()));
			project.setDescription(NormalizeText.normalizeString(project.getDescription()));
			searchedList.add(project);
		}
		return searchedList;
	}

	private Boolean checkUniqueCode(List<Task> tasks, String code) {
		for (Task task : tasks)
			if (task.getCode().equals(code))
				return false;
		return true;
	}
	
	@GetMapping("/tasksView")
	public String getAllTasks(Model model) {
		model.addAttribute("taskViewModel", new TaskViewModel(getAllTasks()));
		Boolean addTaskIsAvailable = false;
		model.addAttribute("addTaskIsAvailable", addTaskIsAvailable);
		return "views/tasksView";
	}

	@PostMapping("/tasksView")
	public String postAllTasks(Model model, @ModelAttribute @Valid TaskDTO taskDTO, BindingResult bindingResult,
			@RequestParam("submit") String reqParam) {
		model.addAttribute("errorMessage", false);
		Boolean addTaskIsAvailable = false;
		model.addAttribute("addTaskIsAvailable", addTaskIsAvailable);
		model.addAttribute("taskViewModel", new TaskViewModel(getAllTasks()));
		model.addAttribute("taskDTO", new TaskDTO());
		switch (reqParam) {
		case "Add":
			addTaskIsAvailable = true;
			model.addAttribute("addTaskIsAvailable", addTaskIsAvailable);
			model.addAttribute("projects", new ProjectViewModel(getAllProjects()));
			break;
		case "Cancel":
			addTaskIsAvailable = false;
			model.addAttribute("addEmployeeIsAvailable", addTaskIsAvailable);
			break;
		case "Save":
			List<Task> tasks = getAllTasks();
			Task task = new Task();
			try {

				if (taskDTO.getCodeTask().isEmpty() || taskDTO.getName().isEmpty()
						|| taskDTO.getDescription().isEmpty()) 
				{
					model.addAttribute("errorMessage", true);
					throw new WrongInputDataException();
				} 
				else 
				{
					Boolean uniqueCodeError = false;
					if (!checkUniqueCode(tasks, taskDTO.getCodeTask())) {
						uniqueCodeError = true;
						model.addAttribute("uniqueCodeError", uniqueCodeError);
						throw new NotUniqueCodeException();
					} else {
						model.addAttribute("uniqueCodeError", uniqueCodeError);
					}
					task.setCode(taskDTO.getCodeTask());
					task.setName(NormalizeText.normalizeString(taskDTO.getName()));
					task.setDescription(NormalizeText.normalizeString(taskDTO.getDescription()));
				}
				tasksRepo.save(task);
				addTaskIsAvailable = false;
				model.addAttribute("addTaskIsAvailable", addTaskIsAvailable);
				model.addAttribute("taskViewModel", new TaskViewModel(getAllTasks()));

			} catch (NotUniqueCodeException | WrongInputDataException e) {
				// TODO: handle exception
			}
			break;
		}
		return "views/tasksView";
	}

}
