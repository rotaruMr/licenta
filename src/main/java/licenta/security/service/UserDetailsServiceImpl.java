package licenta.security.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import licenta.entity.Role;
import licenta.entity.UserTimesheet;
import licenta.repository.UserRepository;
import licenta.security.config.EncrytedPasswordUtils;

@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
	@Autowired
	private UserRepository userRepo;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserTimesheet userTimesheet = getUserByEmail(email);
		if(userTimesheet == null)
		{
			throw new UsernameNotFoundException("Email " + email + " not found in the data base");
		}
		List<Role> roleNames = this.getUserRoles(userTimesheet);
		List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
		List<Role> roles = new ArrayList<>();
		
		if(roleNames != null)
		{
			for(Role role:roles)
				{
					roles.add(role);
					GrantedAuthority authority = new SimpleGrantedAuthority(role.getRole().toString());
					grantList.add(authority);
				}
		}
		for(GrantedAuthority authority: grantList)
		{
			if(authority.getAuthority().toString().equals("employee"))
			{
				throw new UsernameNotFoundException("Email " + email + " not found in the data base");
			}
		}
		UserDetails userDetails = (UserDetails) new User(userTimesheet.getEmail(), EncrytedPasswordUtils.encrytePassword(userTimesheet.getPassword()),
				grantList);
		return userDetails;
	}
	
	private UserTimesheet getUserByEmail(String email)
	{
		Iterable<UserTimesheet> userList = userRepo.findAll();
		for(UserTimesheet user : userList)
		{
			if(user.getEmail().equals(email))
				return user;
		}
		return null;
	}
	
	private List<Role> getUserRoles(UserTimesheet user)
	{
		if(user != null)
			return user.getRoles();
		return null;
	}
}
