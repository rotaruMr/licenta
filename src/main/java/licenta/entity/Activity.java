package licenta.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="activity")
public class Activity {
	
	@Id
	@Column(name="id_activity" , length = 11, nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "id_employee", nullable = false)
	private Employee employee;
	
	@ManyToOne
	@JoinColumn(name = "id_task", nullable = false)
	private Task task;
	
	@Column(name = "date", length = 10, nullable = false, unique = false)
	private String date;
	
	@Column(name = "time_from", length = 10, nullable = false, unique = false)
	private String time_from;
	
	@Column(name = "time_to", length = 10, nullable = false, unique = false)
	private String time_to;
	
	@Column(name = "description", length = 10, nullable = false, unique = false)
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime_from() {
		return time_from;
	}

	public void setTime_from(String time_from) {
		this.time_from = time_from;
	}

	public String getTime_to() {
		return time_to;
	}

	public void setTime_to(String time_to) {
		this.time_to = time_to;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((employee == null) ? 0 : employee.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((task == null) ? 0 : task.hashCode());
		result = prime * result + ((time_from == null) ? 0 : time_from.hashCode());
		result = prime * result + ((time_to == null) ? 0 : time_to.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Activity other = (Activity) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (employee == null) {
			if (other.employee != null)
				return false;
		} else if (!employee.equals(other.employee))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (task == null) {
			if (other.task != null)
				return false;
		} else if (!task.equals(other.task))
			return false;
		if (time_from == null) {
			if (other.time_from != null)
				return false;
		} else if (!time_from.equals(other.time_from))
			return false;
		if (time_to == null) {
			if (other.time_to != null)
				return false;
		} else if (!time_to.equals(other.time_to))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Activity [id=" + id + ", employee=" + employee + ", task=" + task + ", date=" + date + ", time_from="
				+ time_from + ", time_to=" + time_to + ", description=" + description + "]";
	}

	public Activity(Integer id, Employee employee, Task task, String date, String time_from, String time_to,
			String description) {
		super();
		this.id = id;
		this.employee = employee;
		this.task = task;
		this.date = date;
		this.time_from = time_from;
		this.time_to = time_to;
		this.description = description;
	}

	public Activity() {
		super();
	}

	
}
