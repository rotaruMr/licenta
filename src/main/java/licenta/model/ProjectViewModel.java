package licenta.model;

import java.util.List;

import licenta.entity.Employee;
import licenta.entity.Project;

public class ProjectViewModel {

	private List<Project> projectList;

	public List<Project> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<Project> projectList) {
		this.projectList = projectList;
	}

	public ProjectViewModel(List<Project> projectList) {
		super();
		this.projectList = projectList;
	}

	public ProjectViewModel() {
		super();
	}
	
}
