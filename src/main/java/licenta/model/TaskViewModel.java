package licenta.model;

import java.util.List;

import licenta.entity.Task;

public class TaskViewModel {

	private List<Task> taskList;

	public List<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}

	public TaskViewModel() {
		super();
	}

	public TaskViewModel(List<Task> taskList) {
		super();
		this.taskList = taskList;
	}
	
}
