package licenta.model;

import java.util.List;

import licenta.entity.Employee;

public class NewEmployeeViewModel 
{
	private List<Employee> employeeList;

	
	public NewEmployeeViewModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NewEmployeeViewModel(List<Employee> employeeList) {
		super();
		this.employeeList = employeeList;
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}
	
}
